import os
import random



class WordCraft(object):
    def __init__(self):
        with open('words.txt') as fin:
            self.words_all = [i.strip() for i in fin if 6 > len(i) > 2]

    def get_game(self, n=500):
        self.words_now = random.choice(self.words_all, k=n)
